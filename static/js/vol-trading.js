$(document).ready(function(){
    //connect to the socket server.
    let socket = io.connect('http://' + document.domain + ':' + location.port + '/vt');
    let stock_ticker = '';
    let stock_price = 0;
    let stock_position = 0;
    let vol = 0;
    let vol_range_low = 0;
    let vol_range_high = 0;
    let options = [];
    let portfolio_delta = 0;
    let hottest_option = {};
    let arbitrage = [];
    socket.on('tick', function(msg){
        $('#current-tick').html("Current tick: " + msg['current_tick'].toString());
    });

    socket.on('case_tickers', function(msg){
        $('#current-case').html("Current case: " + msg['current_case'].toString());
        stock_ticker = msg['stock_ticker'];
    });

    socket.on('vol_news', function(msg){
        vol = msg['vol'];
        vol_range_low = msg['vol_range_low'];
        vol_range_high = msg['vol_range_high'];
    });

    socket.on('securities', function(msg){
        stock_price = msg['stock_price'];
        stock_position = msg['stock_position'];
        options = msg['options'];
        hottest_option = msg['hot'];
        arbitrage = msg['arbitrage'];
    });

    socket.on('hedging', function(msg){
        portfolio_delta = msg['portfolio_delta'];
    });

    socket.on('update', function(msg){
        let market_dynamics_tbody = '';
        let vol_range_string = !parseFloat(vol_range_high) && !parseFloat(vol_range_low) ? '' : vol_range_low + ' - ' + vol_range_high;
        market_dynamics_tbody +=
            '<tr>' +
            '<td>' + stock_ticker +'</td>' +
            '<td>' + stock_position +'</td>' +
            '<td>' + stock_price +'</td>' +
            '<td>' + vol + '</td>' +
            '<td>' + vol_range_string + '</td>' +
            '</tr>';
        $('#market-dynamics tbody').html(market_dynamics_tbody);

        let securities_tbody = '';
        for (let i = 0; i < options.length; i++) {
            let price_range = !parseFloat(options[i]['vol_low_price']) && !parseFloat(options[i]['vol_high_price'])
                ? ''
                : options[i]['vol_low_price'].toString() + ' - ' + options[i]['vol_high_price'].toString();
            let diff_good_bad = options[i]['diff'] > 0 ? 'good' : 'bad';
            securities_tbody +=
                '<tr>' +
                '<td>' + options[i]['ticker'] +'</td>' +
                '<td>' + options[i]['position'] +'</td>' +
                '<td>' + options[i]['price'].toString() + '</td>' +
                '<td>' + options[i]['bs_price'].toString() + '</td>' +
                '<td class=' + diff_good_bad + '>' + options[i]['diff'].toString() + '</td>' +
                '<td>' + options[i]['delta'].toString() + '</td>' +
                '<td>' + price_range + '</td>' +
                '</tr>';
            $('#securities-analysis tbody').html(securities_tbody);
        }

        let hedging_tbody = '';
        hedging_tbody +=
            '<tr>' +
            '<td>' + portfolio_delta.toString() +'</td>' +
            '<td>' + hottest_option['ticker'] +'</td>' +
            '<td>' + hottest_option['action'] +'</td>' +
            '<td>' + hottest_option['amount'] +'</td>' +
            '<td>' + hottest_option['hedge_amount'] +'</td>' +
            '</tr>';
        $('#hedging-analysis tbody').html(hedging_tbody);

        let arbitrage_tbody = '';
        for (let i = 0; i < arbitrage.length; i++) {
            let call_action = arbitrage[i]['call_action'] === 'Buy' ? 'good' : 'bad';
            let put_action = arbitrage[i]['put_action'] === 'Buy' ? 'good' : 'bad';
            let stock_action = arbitrage[i]['stock_action'] === 'Buy' ? 'good' : 'bad';
            arbitrage_tbody +=
                '<tr>' +
                '<td>' + arbitrage[i]['strike'] +'</td>' +
                '<td>' + arbitrage[i]['diff'] +'</td>' +
                '<td class=' + call_action + '>' + arbitrage[i]['call_action'] + '@' + arbitrage[i]['call'].toString() +'</td>' +
                '<td class=' + put_action + '>' + arbitrage[i]['put_action'] + '@' + arbitrage[i]['put'].toString() + '</td>' +
                '<td class=' + stock_action + '>' + arbitrage[i]['stock_action'] + '@' + arbitrage[i]['stock'].toString() + '</td>' +
                '<td>' + arbitrage[i]['strategy'] +'</td>' +
                '</tr>';
            $('#arbitrage-analysis tbody').html(arbitrage_tbody);
        }
    });
});