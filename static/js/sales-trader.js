$(document).ready(function(){
    //connect to the socket server.
    let socket = io.connect('http://' + document.domain + ':' + location.port + '/snt');
    let ticker_list = [];
    let vol_hist = {};
    let vol_tnt = {};
    let vv_bid = {};
    let vv_ask = {};
    let tender_strategy = [];
    let chicken_dinner = {};

    socket.on('tick', function(msg){
        $('#current-tick').html("Current tick: " + msg['current_tick'].toString());
    });

    socket.on('case_tickers', function(msg) {
        $('#current-case').html("Current case: " + msg['current_case'].toString());
        ticker_list = msg['ticker_list'];
    });

    socket.on('historical_vol', function(msg){
        vol_hist = msg['historical'];
        vol_tnt = msg['TNT'];
    });

    socket.on('volume_velocity', function(msg){
        vv_bid = msg['bid'];
        vv_ask = msg['ask'];
    });

    socket.on('tender_strategy', function(msg){
        if (tender_strategy.length >= 4) {
            tender_strategy.shift()
        }
        tender_strategy.push(msg);
    });

    socket.on('chicken_dinner', function(msg){
        chicken_dinner = msg;
    });

    socket.on('update', function(msg){
        let analytical_tbody = '';
        for (let i = 0; i < ticker_list.length; i++) {
            let bid_up_or_down = vv_bid[ticker_list[i]] > 0 ? 'good' : 'bad';
            let ask_up_or_down = vv_ask[ticker_list[i]] > 0 ? 'good' : 'bad';
            analytical_tbody +=
                '<tr>' +
                '<td>' + ticker_list[i] +'</td>' +
                '<td>' + vol_hist[ticker_list[i]] + '</td>' +
                '<td>' + vol_tnt[ticker_list[i]] + '</td>' +
                '<td class=' + bid_up_or_down + '>' + vv_bid[ticker_list[i]].toString() + '</td>' +
                '<td class=' + ask_up_or_down + '>' + vv_ask[ticker_list[i]].toString() + '</td>' +
                '</tr>'
        }
        $('#real-time-analytics tbody').html(analytical_tbody);

        let strategy_tbody = '';
        for (let i = 0; i < tender_strategy.length; i++) {
            strategy_tbody =
                '<tr>' +
                '<td>' + tender_strategy[i]['ticker'] + '</td>' +
                '<td>' + tender_strategy[i]['type'] + '</td>' +
                '<td>' + tender_strategy[i]['action'] + '</td>' +
                '<td>' + tender_strategy[i]['price'] + '</td>' +
                '<td>' + tender_strategy[i]['quantity'] + '</td>' +
                '<td class="'+ tender_strategy[i]['good_or_bad'] +'">' + tender_strategy[i]['strategy'] + '</td>' +
                '</tr>' + strategy_tbody
        }
        $('#trading-strategy tbody').html(strategy_tbody);

        let chicken_body = Object.keys(chicken_dinner).length === 0 && chicken_dinner.constructor === Object ? '' :
            '<tr>' +
            '<td>' + chicken_dinner['ticker'] + '</td>' +
            '<td>' + chicken_dinner['type'] + '</td>' +
            '<td>' + chicken_dinner['action'] + '</td>' +
            '<td>' + chicken_dinner['price'] + '</td>' +
            '<td>' + chicken_dinner['quantity'] + '</td>' +
            '<td>' + chicken_dinner['strategy'] + '</td>' +
            '</tr>';
        $('#chicken-dinner tbody').html(chicken_body);
    });
});