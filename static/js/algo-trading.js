$(document).ready(function(){
    //connect to the socket server.
    let socket = io.connect('http://' + document.domain + ':' + location.port + '/at');
    let stock_ticker = '';
    socket.on('tick', function(msg){
        $('#current-tick').html("Current tick: " + msg['current_tick'].toString());
    });

    socket.on('case_tickers', function(msg){
        $('#current-case').html("Current case: " + msg['current_case'].toString());
        stock_ticker = msg['stock_ticker'];
    });
});