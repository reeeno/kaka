from flask import Flask, request, jsonify
import requests

app = Flask(__name__)


@app.route("/v1/securities/<something>", methods=['GET'])
def securities(something):
    return jsonify(requests.get("http://localhost:9999/v1/securities/{}?key={}&ticker={}"
                                .format(something, request.args.get('key', ''), request.args.get('ticker', ''))).json())


@app.route("/v1/<endpoint>/<something>", methods=['GET'])
def generic_two_level(endpoint, something):
    return jsonify(requests.get("http://localhost:9999/v1/{}/{}?key={}"
                                .format(endpoint, something, request.args.get('key', ''))).json())


@app.route("/v1/<endpoint>", methods=['GET'])
def generic_one_level(endpoint):
    return jsonify(requests.get("http://localhost:9999/v1/{}?key={}"
                                .format(endpoint, request.args.get('key', ''))).json())


@app.route("/")
def hello():
    return "Hello World!"

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)