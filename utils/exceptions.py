class KakaException(Exception):
    """Base exception
    """


class EnumException(KakaException):
    """Doesn't match Enum
    """
