import requests
from config.constants import API_PARAMS
from collections import ChainMap


def get_response(api, *args):
    # *args needs to be in the format of dictionaries. those will be additional parameters
    if not args:
        return requests.get(api, params=API_PARAMS).json()
    else:
        return requests.get(api, params={**API_PARAMS, **dict(ChainMap(*args))}).json()


def post_request(api, *args):
    # *args needs to be in the format of dictionaries. those will be additional parameters
    if not args:
        return requests.post(api, params=API_PARAMS).json()
    else:
        return requests.post(api, params={**API_PARAMS, **dict(ChainMap(*args))}).json()


def delete_request(api, *args):
    # *args needs to be in the format of dictionaries. those will be additional parameters
    if not args:
        return requests.delete(api, params=API_PARAMS).json()
    else:
        return requests.delete(api, params={**API_PARAMS, **dict(ChainMap(*args))}).json()
