from math import *
from scipy.stats import norm
from .exceptions import EnumException


class Option:
    """
    Synopsis:
    Calculate Delta, Gamma, Theta, Vega, Rho for call and put european options
    Some assumptions of these calculations are:
    - ignores any dividends paid during the option's lifetime
    - can only be exercised at expiration
    - efficient markets and no commissions
    - risk-free rate and volatility of the underlying asset are known and constant
    - returns follow a normal distribution

    Input:
    - spot and strike price of the underlying asset
    - time in years
    - annualized volatility
    - compounded risk free rate
    - type of the option (put/call)

    Output:
    - Delta, Gamma, Theta, Vega and Rho values of call and put european options using the Black-Scholes formulas
    """

    d1 = 0
    d2 = 0
    spot = 0
    strike = 0
    time = 0
    risk_free_rate = 0
    sigma = 0
    greek_delta = 0
    greek_gamma = 0
    greek_theta = 0
    greek_rho = 0
    greek_vega = 0
    bs_price = 0
    call_put = ''  # 'P' or 'C'
    
    def __init__(self, spot, strike, time, risk_free_rate, sigma, call_put):
        self.spot = spot  # Spot price
        self.strike = strike  # Strike price
        self.time = time  # Time to maturity
        self.risk_free_rate = risk_free_rate  # Risk free rate
        self.sigma = sigma  # Volatility of underlying asset
        self.call_put = call_put  # Call or Put option
        self.calculate_d1()  # Calculate N(d1)
        self.calculate_d2()  # Calculate N(d2)

        # d1num = (log(spot / strike) + (risk_free_rate + 0.5 * sigma * sigma) * time)

    def set_spot(self, spot):
        self.spot = spot
        self.calculate_d1()
        self.calculate_d2()

    def set_strike(self, strike):
        self.strike = strike
        self.calculate_d1()
        self.calculate_d2()

    def set_time(self, time):
        self.time = time
        self.calculate_d1()
        self.calculate_d2()

    def set_risk_free_rate(self, risk_free_rate):
        self.risk_free_rate = risk_free_rate
        self.calculate_d1()
        self.calculate_d2()

    def set_sigma(self, sigma):
        self.sigma = sigma
        self.calculate_d1()
        self.calculate_d2()

    def set_put_call(self, put_call):
        self.call_put = put_call

    def calculate_d1(self):
        self.d1 = (log(self.spot / self.strike) + (self.risk_free_rate + 0.5 * self.sigma * self.sigma) * self.time) \
                  / (self.sigma * sqrt(self.time))
        return self.d1

    def calculate_d2(self):
        self.d2 = self.d1 - self.sigma * sqrt(self.time)
        return self.d2

    def calculate_bs_price(self):
        if self.call_put == 'C':
            self.bs_price = self.spot * norm.cdf(self.d1) - self.strike * exp(-self.risk_free_rate * self.time) * \
                            norm.cdf(self.d2)
        elif self.call_put == 'P':
            self.bs_price = self.strike * exp(-self.risk_free_rate * self.time) * norm.cdf(-self.d2) - self.spot * \
                            norm.cdf(-self.d1)
        else:
            raise EnumException
        return self.bs_price

    def calculate_delta(self):
        if self.call_put == 'C':
            self.greek_delta = norm.cdf(self.d1)
        elif self.call_put == 'P':
            self.greek_delta = norm.cdf(self.d1 - 1)
        else:
            raise EnumException
        return self.greek_delta

    def calculate_gamma(self):
        self.greek_gamma = norm.pdf(self.d1) / (self.spot * self.sigma * sqrt(self.time))
        return self.greek_gamma

    def calculate_theta(self):
        if self.call_put == 'C':
            self.greek_theta = -(self.spot * norm.pdf(self.d1) * self.sigma / (2 * sqrt(self.time))) - (
                    self.risk_free_rate * self.strike * exp(-self.risk_free_rate * self.time) * norm.cdf(self.d2))
        elif self.call_put == 'P':
            self.greek_theta = -(self.spot * norm.pdf(self.d1) * self.sigma / (2 * sqrt(self.time))) + (
                    self.risk_free_rate * self.strike * exp(-self.risk_free_rate * self.time) * norm.cdf(-self.d2))
        else:
            raise EnumException
        return self.greek_theta

    def calculate_rho(self):
        if self.call_put == 'C':
            self.greek_rho = (self.time * self.strike * exp(-self.risk_free_rate * self.time) * norm.cdf(self.d2))
        elif self.call_put == 'P':
            self.greek_rho = (-self.time * self.strike * exp(-self.risk_free_rate * self.time) * norm.cdf(-self.d2))
        else:
            raise EnumException
        return self.greek_rho

    def calculate_vega(self):
        self.greek_vega = (self.spot * norm.pdf(self.d1) * sqrt(self.time))
        return self.greek_vega
