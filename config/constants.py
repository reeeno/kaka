# Constants
# BASE_URL = "http://10.88.111.7:5000/v1"
BASE_URL = "http://localhost:9999/v1"
API_KEY = '1C1OFJGC'  # Home
# API_KEY = '3HDZAM6P'
API_CASE = BASE_URL + '/case'
API_TRADER = BASE_URL + '/trader'
API_LIMITS = BASE_URL + '/limits'
API_NEWS = BASE_URL + '/news'
API_ASSETS = BASE_URL + '/assets'
API_SECURITIES = BASE_URL + '/securities'
API_SEC_BOOK = BASE_URL + '/securities/book'
API_SEC_HIST = BASE_URL + '/securities/history'
API_ORDERS = BASE_URL + '/orders'
API_TENDERS = BASE_URL + '/tenders'
API_BULK_CANCEL = BASE_URL + '/commands/cancel'
API_PARAMS = {
    "key": API_KEY
}

CONSTANT_NORM_PERT_40 = 0.2533471031357997987982
CONSTANT_NORM_PERT_25 = 0.67448975
CONSTANT_NORM_PERT_10 = 1.2815515655446
