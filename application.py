from flask_socketio import SocketIO
from flask import Flask, render_template
from threading import Thread, Event
from config.constants import *
from statistics import stdev, StatisticsError
import math
from functools import reduce
import re
from requests.exceptions import ConnectionError

from utils.helpers import get_response, post_request, delete_request
from utils.quant_lib import Option
from utils.util import dcopy

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
app.config['DEBUG'] = False
socketio = SocketIO(app)

# Random number generator thread
thread = Thread()
thread_stop_event = Event()
client_connect_event = Event()


class SNTThread(Thread):

    def __init__(self):
        self.delay = 0.3
        self.current_tick = 0
        self.current_case = ''
        self.ticker_list = []
        self.price_history = {}
        # Books
        self.books = []
        self.book_look_back_size = 4
        # Volatility
        self.volatility = {"historical": {}, "TNT": {}}
        self.vol_trailing_size = 5
        self.annualized_vol = 0
        # Liquidity
        self.volume_velocity = {"bid": {}, "ask": {}}
        # Tenders
        self.tenders = []
        self.tenders_size = 3
        self.tender_id_list = []
        self.new_tender_arrived = False
        self.future_ticks_size = 3
        self.chicken_dinner = {}  # Keep Winner-Take-All tender alive for 30 seconds
        self.chicken_counter = 30
        super(SNTThread, self).__init__()

    @staticmethod
    def send_update(msg_name, msg_body, namespace='/snt'):
        socketio.emit(msg_name, msg_body, namespace=namespace)

    def sync_tick(self):
        """
        Sync thread tick with server tick. Returns thread tick if updated, or -1 if current thread tick
        equals to server tick
        """
        response = get_response(API_CASE)
        if self.current_tick == response['tick']:
            return -1
        else:
            self.current_tick = response['tick']
            return self.current_tick

    def get_update_case_tickers(self):
        """
        Emits message to updates current case and ticker list in the jumbotron
        """
        self.current_case = get_response(API_CASE)['name']
        self.ticker_list = list(map(lambda x: x['ticker'], get_response(API_SECURITIES)))
        socketio.emit('case_tickers', {'current_case': self.current_case, 'ticker_list': self.ticker_list},
                      namespace='/snt')

    def get_price_history(self):
        """
        Get price history for all tickers. {"ticker1": [stock_price1], "ticker2": [stock_price2]}
        """
        for ticker in self.ticker_list:
            ticker_param = {'ticker': ticker}
            self.price_history[ticker] = list(map(lambda x: x['close'], get_response(API_SEC_HIST, ticker_param)))

    def update_historical_vol(self):
        """
        Calculates historical volatility for all tickers. Emit message.
        """
        self.get_price_history()
        for ticker in self.ticker_list:
            ph = self.price_history[ticker]  # Note that price_history is ordered from newest to oldest
            return_series = list(map(lambda x: (x[0] - x[1]) / x[1], zip(ph[:-1], ph[1:])))
            try:
                # Annualize and output as string with % sign
                # Could really just put 86.94826047714 here instead of math.sqrt(252 * 30) to improve performance
                self.volatility['historical'][ticker] = "{0:.4%}".format(
                    stdev(return_series) * pow(math.sqrt(252 * 30), self.annualized_vol))
                self.volatility['TNT'][ticker] = "{0:.4%}".format(
                    stdev(return_series[:self.vol_trailing_size]) * pow(math.sqrt(252 * 30), self.annualized_vol))
            except StatisticsError:
                self.volatility['historical'][ticker] = "{0:.4%}".format(0)  # Handle the first tick
                self.volatility['TNT'][ticker] = "{0:.4%}".format(0)
        socketio.emit('historical_vol', self.volatility, namespace='/snt')

    def get_books(self):
        """
        Get latest 4 books for all tickers. [{ticker1: {"bid": [{"price": 10, "size": 1000}], "ask": [{}]},
                                              ticker2: {"bid: [{"price": 10, "size": 1000}], "ask": [{}]}}]
        """
        book = {}
        for ticker in self.ticker_list:
            ticker_param = {'ticker': ticker}
            response = get_response(API_SEC_BOOK, ticker_param)
            book[ticker] = {
                'bid': list(map(lambda x: {'price': x['price'], 'size': x['quantity'] - x['quantity_filled']},
                                response['bids'])),
                'ask': list(map(lambda x: {'price': x['price'], 'size': x['quantity'] - x['quantity_filled']},
                                response['asks']))
            }
        if len(self.books) > self.book_look_back_size:
            self.books.pop(0)
        self.books.append(book)

    def update_volume_velocity(self):
        """
         Calculates volume velocity as the average change in volume for bid/ask over the past 3 ticks. Emit message.
        """
        self.get_books()
        for ticker in self.ticker_list:
            bid_volumes = list(map(sum, list(map(lambda x: list(map(lambda z: z['size'], x)),
                                                 list(map(lambda y: y[ticker]['bid'], self.books))))))
            ask_volumes = list(map(sum, list(map(lambda x: list(map(lambda z: z['size'], x)),
                                                 list(map(lambda y: y[ticker]['ask'], self.books))))))
            try:
                bid_volume_change = list(map(lambda x: x[1] - x[0], zip(bid_volumes[:-1], bid_volumes[1:])))
                ask_volume_change = list(map(lambda x: x[1] - x[0], zip(ask_volumes[:-1], ask_volumes[1:])))
                # volume_velocity['bid'][ticker] = "{:.4f}".format(int(sum(bid_volume_change) / len(bid_volume_change)))
                # volume_velocity['ask'][ticker] = "{:.4f}".format(int(sum(ask_volume_change) / len(ask_volume_change)))
                self.volume_velocity['bid'][ticker] = int(sum(bid_volume_change) / len(bid_volume_change))
                self.volume_velocity['ask'][ticker] = int(sum(ask_volume_change) / len(ask_volume_change))
            except ZeroDivisionError:
                self.volume_velocity['bid'][ticker] = 0
                self.volume_velocity['ask'][ticker] = 0
        socketio.emit('volume_velocity', self.volume_velocity, namespace='/snt')

    def get_tenders(self):
        """
        Get active tender offers. Keep a history of 3 tender offers, parameters and their trading strategies.
        """
        response = get_response(API_TENDERS)
        try:
            active_tender = response[0]
            if active_tender['tender_id'] not in self.tender_id_list:
                if active_tender['is_fixed_bid']:
                    tender_type = 'Private'
                elif active_tender['caption'][:6] == 'Please':
                    tender_type = 'Competitive'
                else:
                    tender_type = 'Winner-Take-All'
                parsed_tender = {
                    "id": active_tender['tender_id'],
                    'ticker': active_tender['ticker'],
                    'quantity': active_tender['quantity'],
                    'action': active_tender['action'],
                    'price': active_tender['price'],
                    'type': tender_type
                }
                if len(self.tenders) > self.tenders_size:
                    self.tenders.pop(0)
                    self.tender_id_list.pop(0)
                self.tenders += [parsed_tender]
                self.tender_id_list += [active_tender['tender_id']]
                self.new_tender_arrived = True
                return parsed_tender
        except IndexError:  # No active tender offer
            pass

    # TODO: Don't keep track of past 3 tenders, only look at active ones, evaluate active tenders at each tick
    def update_tender_strategy(self):
        """
        Update current tender list and suggest trading strategy
        """

        def determine_price(order_list, threshold_volume):  # Determine what price can provide a volume
            acc = 0
            p = 0
            for i in order_list:
                p = i['price']
                acc += i['size']
                if acc > threshold_volume:
                    return p
            return p

        new_tender = self.get_tenders()
        # Handle chicken dinner first
        if self.chicken_dinner:
            chicken_dinner = self.chicken_dinner
            price = chicken_dinner['price']
            quantity = chicken_dinner['quantity']
            action = chicken_dinner['action']
            ticker = chicken_dinner['ticker']
            curr_book = self.books[len(self.books) - 1][ticker]
            if action == 'SELL':
                bid_or_ask = 'ask'
                direction = 1  # price goes up = bad boy
            else:  # BUY
                bid_or_ask = 'bid'
                direction = -1  # price goes down = bad boy
            future_price_change = (CONSTANT_NORM_PERT_40 * float(self.volatility['historical'][ticker][:-1]) / 100 *
                                   self.future_ticks_size)
            backward_current_volume = quantity - self.volume_velocity[bid_or_ask][ticker] * self.future_ticks_size

            stressed_curr_book = list(
                map(lambda x: {"price": direction * (x['price'] + direction * future_price_change),
                               "size": x['size']},
                    curr_book[bid_or_ask]))

            some_price = determine_price(stressed_curr_book, backward_current_volume)

            chicken_dinner['strategy'] = 'Bid at ' + "{:.2f}".format(some_price)

            socketio.emit('chicken_dinner', chicken_dinner, namespace='/snt')
            self.chicken_dinner = chicken_dinner

            self.chicken_counter -= 1
            if self.chicken_counter == 0:  # Reset chicken dinner
                self.chicken_counter = 30
                self.chicken_dinner = {}
        else:  # Empty chicken dinner. Clears front end
            socketio.emit('chicken_dinner', self.chicken_dinner, namespace='/snt')

        if self.new_tender_arrived:
            try:
                price = new_tender['price']
                quantity = new_tender['quantity']
                action = new_tender['action']
                ticker = new_tender['ticker']
                tender_type = new_tender['type']

                if tender_type == 'Private':
                    # look in book and see if current volume + price + changes in next 3 ticks for volume + price can
                    # support this offer. Changes in price is predicted using worst 25th percentile on normal
                    # distribution
                    curr_book = self.books[len(self.books) - 1][ticker]
                    if action == 'SELL':
                        bid_or_ask = 'ask'
                        direction = 1  # price goes up = bad boy
                    else:  # BUY
                        bid_or_ask = 'bid'
                        direction = -1  # price goes down = bad boy
                    future_price_change = (CONSTANT_NORM_PERT_25 * float(self.volatility['historical'][ticker][:-1]) / 100 *
                                           self.future_ticks_size)
                    try:
                        tmp = list(filter(lambda x: direction * (x['price'] + direction * future_price_change -
                                                                 price) <= 0, curr_book[bid_or_ask]))
                        stressed_curr_volume = reduce(lambda y, z: y + z['size'], [0] + tmp)
                        # Handles weird behaviour of reduce when reducing a list of one dictionary
                        if isinstance(stressed_curr_volume, dict):
                            stressed_curr_volume = stressed_curr_volume['size']
                    except TypeError:
                        stressed_curr_volume = 0
                    stressed_future_volume = stressed_curr_volume + self.volume_velocity[bid_or_ask][ticker] * self.future_ticks_size

                    if stressed_future_volume >= quantity:
                        new_tender['good_or_bad'] = 'good'
                        new_tender['strategy'] = 'Accept'
                    else:
                        new_tender['good_or_bad'] = 'bad'
                        new_tender['strategy'] = 'Decline'
                elif tender_type == 'Competitive':
                    # Use same logic as in Private offering but stress the price harder
                    # (10 percentile adverse for next 3 ticks)
                    curr_book = self.books[len(self.books) - 1][ticker]
                    if action == 'SELL':
                        bid_or_ask = 'ask'
                        direction = 1  # price goes up = bad boy
                    else:  # BUY
                        bid_or_ask = 'bid'
                        direction = -1  # price goes down = bad boy
                    future_price_change = (CONSTANT_NORM_PERT_10 * float(self.volatility['historical'][ticker][:-1]) / 100 *
                                           self.future_ticks_size)
                    backward_current_volume = quantity - self.volume_velocity[bid_or_ask][ticker] * self.future_ticks_size

                    stressed_curr_book = list(map(lambda x: {"price": x['price'] + direction * future_price_change,
                                                             "size": x['size']},
                                                  curr_book[bid_or_ask]))

                    some_price = determine_price(stressed_curr_book, backward_current_volume)

                    new_tender['good_or_bad'] = 'good'
                    new_tender['strategy'] = 'Bid at ' + "{:.2f}".format(some_price)
                else:  # tender_type == 'Winner-Take-All'
                    self.chicken_dinner = new_tender
                    return  # Prevent emitting message

                socketio.emit('tender_strategy', new_tender, namespace='/snt')
                self.new_tender_arrived = False
            except TypeError:  # Where most recent active tender is a winner-take-all
                pass

    @staticmethod
    def signal_page_update():
        """
        Emits a socketio message to signal client front end to update page content
        """
        socketio.emit("update", {}, namespace='/snt')

    def run(self):
        print("Initializing thread")
        while not thread_stop_event.isSet():
            if client_connect_event.isSet():  # Initial connect/new case
                self.get_update_case_tickers()
                client_connect_event.clear()

            if self.sync_tick() > 0:  # Per tick update
                # Update tick
                socketio.emit('tick', {'current_tick': self.current_tick}, namespace='/snt')

                # Update historical volatility
                self.update_historical_vol()

                # Update volume velocity
                self.update_volume_velocity()

                # Update Tenders and Trading Strategy
                self.update_tender_strategy()

                # Signal client front end to update content
                self.signal_page_update()

            if self.current_tick == 0:  # A new case started
                client_connect_event.set()
            socketio.sleep(self.delay)


class VTThread(Thread):

    def __init__(self):
        self.delay = 0.3
        self.current_tick = 0
        self.current_case = ''
        self.news_id_list = []
        self.current_delta_limit = 0
        # Volatility
        # NOTE: ALL BELOW VOLATILITIES ARE ANNUALIZED. Divide by math.sqrt(252 * 30) to get tick volatility
        self.vol = 0
        self.vol_range_low = 0
        self.vol_range_high = 0
        # Securities
        self.stock_ticker = ''
        self.stock_price = 0
        self.stock_position = 0
        self.options = []  # [{ticker, position, price, delta, bs_price, vol_low_price, vol_high_price, diff}]
        self.time_to_maturity = 0  # for the sake of option pricing
        # Hedging
        self.portfolio_delta = 0
        self.delta_threshold = 0.7
        self.option_trade_size = 20
        self.stock_trade_size = 100
        super(VTThread, self).__init__()

    def sync_tick(self):
        """
        Sync thread tick with server tick. Returns thread tick if updated, or -1 if current thread tick
        equals to server tick
        """
        response = get_response(API_CASE)
        if self.current_tick == response['tick']:
            return -1
        else:
            self.current_tick = response['tick']
            self.time_to_maturity = (600 - self.current_tick) / 252 / 30  # 252 trading days per year, 30 ticks per day
            return self.current_tick

    def get_update_case_tickers(self):
        """
        Emits message to updates current case and ticker list in the jumbotron
        """
        self.current_case = get_response(API_CASE)['name']
        self.get_securities()
        # TODO: Read heat specific delta limit and show in header
        socketio.emit('case_tickers', {'current_case': self.current_case, 'stock_ticker': self.stock_ticker}, namespace='/vt')

    def get_update_vol_news(self):
        """
        Emits message to update true volatility/forcasted volatility range
        """
        # Only query news items when current tick is n * 75 +/- 3, or initially update volatility.
        temp_mod = self.current_tick % 75
        if temp_mod <= 3 or temp_mod > 72 or self.vol == 0:
            # Only read most recent news
            full_response = get_response(API_NEWS)
            response = full_response[0]
            if response['news_id'] not in self.news_id_list:
                self.news_id_list += [response['news_id']]
                if response['tick'] == 0:  # special treatment for initial announcement
                    try:
                        self.vol = int(re.search('realized volatility is (.+?)%', response['body']).group(1)) / 100
                    except AttributeError:
                        self.vol = 0
                elif response['ticker'] == '':  # Volatility forecast
                    try:
                        vol_range = re.search('next week will be between (.+?) at the start', response['body']).group(1)
                        self.vol_range_low = int(re.search('(.+?)% ~', vol_range).group(1)) / 100
                        self.vol_range_high = int(re.search('~ (.+?)%', vol_range).group(1)) / 100
                    except AttributeError:
                        self.vol_range_low = 0
                        self.vol_range_high = 0
                else:  # True volatility announcement
                    try:
                        self.vol = int(re.search('this week will be (.+?)%', response['body']).group(1)) / 100
                    except AttributeError:
                        self.vol = 0
                    # When true volatility is announced, set forecast volatility to 0 since they are not really useful
                    self.vol_range_low = 0
                    self.vol_range_high = 0
                # Handle case where tread start at the middle of case and latest news was a forecast
                if self.vol == 0:
                    one_more = full_response[1]
                    if one_more['tick'] == 0:  # special treatment for initial announcement
                        try:
                            self.vol = int(re.search('realized volatility is (.+?)%', one_more['body']).group(1)) / 100
                        except AttributeError:
                            self.vol = 0
                    else:  # True volatility announcement
                        try:
                            self.vol = int(re.search('this week will be (.+?)%', one_more['body']).group(1)) / 100
                        except AttributeError:
                            self.vol = 0
                socketio.emit('vol_news', {'vol': "{0:.2%}".format(self.vol),
                                           'vol_range_low': "{0:.2%}".format(self.vol_range_low),
                                           'vol_range_high': "{0:.2%}".format(self.vol_range_high)},
                              namespace='/vt')

    def get_securities(self):
        """
        Get latest stock/option prices
        """
        ticker_list = list(map(lambda x: (x['type'], x['ticker'], x['last'], x['position']), get_response(
            API_SECURITIES)))
        self.stock_ticker, self.stock_price, self.stock_position = list(filter(lambda y: y[0] == 'STOCK', ticker_list))[0][1:4]
        self.options = list(
            map(lambda x: {'ticker': x[1], 'price': x[2], 'position': x[3]}, list(filter(lambda y: y[0] == 'OPTION', ticker_list))))

    def update_securities(self):
        """
        Emit message to update front end with calculated securities information
        """
        self.get_securities()
        for option in self.options:
            call_put = option['ticker'][-1:]
            strike = float(option['ticker'][-3:-1])
            try:
                o = Option(self.stock_price, strike, self.time_to_maturity, 0, self.vol, call_put)
            except ZeroDivisionError:
                break  # Case ended
            option['bs_price'] = o.calculate_bs_price()
            option['delta'] = o.calculate_delta() * 100
            if self.vol_range_high and self.vol_range_low:
                # Calculate BS price using forecasted volatility range
                o.set_sigma(self.vol_range_low)
                option['vol_low_price'] = o.calculate_bs_price()
                o.set_sigma(self.vol_range_high)
                option['vol_high_price'] = o.calculate_bs_price()
            else:
                option['vol_low_price'] = 0
                option['vol_high_price'] = 0
            option['diff'] = option['bs_price'] - option['price']

        # format the numbers in the output send to front end
        output_options = dcopy(self.options)
        for oo in output_options:
            oo['bs_price'] = "{:.2f}".format(oo['bs_price'])
            oo['delta'] = "{:.4f}".format(oo['delta'])
            oo['vol_high_price'] = "{:.2f}".format(oo['vol_high_price'])
            oo['vol_low_price'] = "{:.2f}".format(oo['vol_low_price'])
            oo['diff'] = "{:.2f}".format(oo['diff'])

        # Sort options by abs(diff) descending then for the options that has abs(diff) within highest value +/- .01,
        # sort by delta ascending
        sorted_options = sorted(self.options, key=lambda item: abs(item['diff']), reverse=True)
        biggest_dick = abs(sorted_options[0]['diff'])
        big_dick_options = list(filter(lambda item: abs(item['diff']) >= biggest_dick - 0.01, sorted_options))
        small_balls = sorted(big_dick_options, key=lambda item: abs(item['delta']))
        hot = small_balls[0]

        # Spot arbitrage opportunity using put-call parity
        # C - P = S - K
        arbitrage = {}
        for o in self.options:
            call_put = o['ticker'][-1:]
            strike = o['ticker'][-3:-1]  # this guy is a string!!!
            if strike in arbitrage.keys():
                arbitrage[strike][call_put] = o['price']
            else:
                arbitrage[strike] = {call_put: o['price']}

        # {strike, call, put, stock, strategy}
        arbitrage_opportunities = []
        for s in arbitrage:
            arbitrage_diff = arbitrage[s]['C'] - arbitrage[s]['P'] - self.stock_price + float(s)
            if arbitrage_diff == 0:
                pass
            elif arbitrage_diff > 0:
                arbitrage_opportunities.append({
                    'strike': int(s),
                    'call': "{:.2f}".format(arbitrage[s]['C']),
                    'put': "{:.2f}".format(arbitrage[s]['P']),
                    'stock': "{:.2f}".format(self.stock_price),
                    'call_action': 'Sell',
                    'put_action': 'Buy',
                    'stock_action': 'Buy',
                    "strategy": "Short Synth | Long Stock",
                    "diff": "{:.2f}".format(arbitrage_diff)
                })
            else:
                arbitrage_opportunities.append({
                    'strike': int(s),
                    'call': "{:.2f}".format(arbitrage[s]['C']),
                    'put': "{:.2f}".format(arbitrage[s]['P']),
                    'stock': "{:.2f}".format(self.stock_price),
                    'call_action': 'Buy',
                    'put_action': 'Sell',
                    'stock_action': 'Sell',
                    "strategy": "Long Synth | Short Stock", 
                    "diff": "{:.2f}".format(arbitrage_diff)
                })
        arbitrage_opportunities.sort(key=lambda item: abs(float(item['diff'])), reverse=True)
        # Calculate amount of contracts I can take before delta going over the roof, and the hedging amount
        delta_limit = self.delta_threshold * self.current_delta_limit if self.delta_threshold else 1000 * self.current_delta_limit
        hot['amount'] = delta_limit / hot['delta'] - delta_limit / hot['delta'] % self.option_trade_size
        hot['action'] = 'Buy' if hot['diff'] >= 0 else 'Sell'
        # Hedge delta created by this trade
        # total_delta = self.portfolio_delta + hot['amount'] * hot['delta']
        total_delta = hot['amount'] * hot['delta']
        hot['hedge_amount'] = total_delta - total_delta % self.stock_trade_size

        socketio.emit('securities', {'stock_price': self.stock_price, 'stock_position': self.stock_position,
                                     'options': output_options, 'hot': hot, 'arbitrage': arbitrage_opportunities},
                      namespace='/vt')

    def update_portfolio_delta(self):
        """
        Emit message to update portfolio delta
        """
        self.portfolio_delta = 1 * self.stock_position + reduce(lambda x, y: x + y['position'] * y['delta'],
                                                                self.options, 0)
        socketio.emit('hedging', {'portfolio_delta': '{:.2f}'.format(self.portfolio_delta)}, namespace='/vt')

    @staticmethod
    def signal_page_update():
        """
        Emits a socketio message to signal client front end to update page content
        """
        socketio.emit("update", {}, namespace='/vt')

    def run(self):
        print("Initializing thread")
        while not thread_stop_event.isSet():
            if client_connect_event.isSet():  # Initial connect/new case
                self.get_update_case_tickers()
                # Re-iterate on the volatility
                socketio.emit('vol_news', {'vol': "{0:.2%}".format(self.vol),
                                           'vol_range_low': "{0:.2%}".format(self.vol_range_low),
                                           'vol_range_high': "{0:.2%}".format(self.vol_range_high)},
                              namespace='/vt')
                client_connect_event.clear()

            if self.sync_tick() > 0:  # Per tick update
                # Update tick
                socketio.emit('tick', {'current_tick': self.current_tick}, namespace='/vt')

                # Update volatility
                self.get_update_vol_news()

                # Update securities
                self.update_securities()

                # Update portfolio delta
                self.update_portfolio_delta()

                # Signal client front end to update content
                self.signal_page_update()

            if self.current_tick == 0:  # A new case started
                client_connect_event.set()
            socketio.sleep(self.delay)


class ATThread(Thread):

    def __init__(self):
        self.delay = 0.3
        self.current_tick = 0
        self.current_case = ''
        self.ticker_list = []
        self.book = {}
        self.securities = []
        self.orders = []
        self.limits = {}
        self.liquidation_attempt = 0
        super(ATThread, self).__init__()

    def sync_tick(self):
        """
        Sync thread tick with server tick. Returns thread tick if updated, or -1 if current thread tick
        equals to server tick
        """
        response = get_response(API_CASE)
        if self.current_tick == response['tick']:
            return -1
        else:
            self.current_tick = response['tick']
            return self.current_tick

    def get_update_case_tickers(self):
        """
        Emits message to updates current case and ticker list in the jumbotron
        """
        self.current_case = get_response(API_CASE)['name']
        self.ticker_list = list(map(lambda x: x['ticker'], get_response(API_SECURITIES)))
        socketio.emit('case_tickers', {'current_case': self.current_case, 'ticker_list': self.ticker_list},
                      namespace='/at')

    def get_book(self):
        """
        Get latest stock/option prices
        """
        for ticker in self.ticker_list:
            ticker_param = {'ticker': ticker}
            response = get_response(API_SEC_BOOK, ticker_param)
            self.book[ticker] = {
                'bid': list(map(lambda x: {'price': x['price'], 'size': x['quantity'] - x['quantity_filled'],
                                           'trade_id': x['trader_id']}, response['bids'])),
                'ask': list(map(lambda x: {'price': x['price'], 'size': x['quantity'] - x['quantity_filled'],
                                           'trade_id': x['trader_id']}, response['asks']))
            }

    def get_securities(self):
        securities = get_response(API_SECURITIES)
        self.securities = []
        for sec in securities:
            self.securities.append({
                'ticker': sec['ticker'],
                'last': sec['last'],
                'bid': sec['bid'],
                'bid_size': sec['bid_size'],
                'ask': sec['ask'],
                'ask_size': sec['ask_size'],
                'position': sec['position'],
                'weight': 1 / sec['limits'][0]['units'],
                'cost': sec['vwap']
            })

    def get_limits(self):
        self.limits = get_response(API_LIMITS)[0]
        self.limits['gross_utilization'] = 0 if not self.limits['gross_limit'] else abs(self.limits['gross']) / self.limits['gross_limit']
        self.limits['net_utilization'] = 0 if not self.limits['net_limit'] else abs(self.limits['net']) / self.limits[
            'net_limit']

    def get_orders(self):
        self.orders = get_response(API_ORDERS)

    def send_order(self, ticker, order_type, action, quantity, price=0):
        order = {
            'ticker': ticker,
            'type': order_type,
            'action': action,
            'price': price,
            'quantity': quantity
        }
        if order['ticker'] in ['BEAV', 'MAPL', 'COLD'] and order['quantity'] > 10000:
            num_needed = (order['quantity'] - order['quantity'] % 10000) / 10000
            last_order = order['quantity'] % 10000
            for i in range(0, int(num_needed)):
                order['quantity'] = 10000
                self.orders.append(post_request(API_ORDERS, order))
            order['quantity'] = last_order
            self.orders.append(post_request(API_ORDERS, order))
        elif order['ticker'] == 'CND' and order['quantity'] > 5000:
            num_needed = (order['quantity'] - order['quantity'] % 5000) / 5000
            last_order = order['quantity'] % 5000
            for i in range(0, int(num_needed)):
                order['quantity'] = 5000
                self.orders.append(post_request(API_ORDERS, order))
            order['quantity'] = last_order
            self.orders.append(post_request(API_ORDERS, order))
        else:
            self.orders.append(post_request(API_ORDERS, order))
        # print(order['action'].capitalize() + 'ing ' + order['ticker'] + ' @ ' + str(price) + ' for ' + str(quantity))

    @staticmethod
    def cancel_order(order_id):
        # print("Cancelling order id " + str(order_id))
        response = delete_request(API_ORDERS + '/' + str(order_id))
        if "success" in response.keys():
            return True
        else:
            return False

    def algo_trading(self):
        order_list = []
        # # Arbitrage with ETF
        # beav = {}
        # mapl = {}
        # cold = {}
        # cnd = {}
        # for sec in self.securities:
        #     if sec['ticker'] == 'BEAV':
        #         beav = sec
        #     elif sec['ticker'] == 'MAPL':
        #         mapl = sec
        #     elif sec['ticker'] == 'COLD':
        #         cold = sec
        #     elif sec['ticker'] == 'CND':
        #         cnd = sec
        # if cnd['bid'] - beav['ask'] - 2 * mapl['ask'] - 2 * cold['ask'] > 0:
        #     print("Arbitraging with ETF!")
        #     size = (self.limits['gross_limit'] - self.limits['gross']) / 10
        #     order_list.append({
        #         'ticker': 'CND',
        #         'action': 'SELL',
        #         'price': cnd['bid'],
        #         'size': size,
        #         'type': 'arbitrage'
        #     })
        #     order_list.append({
        #         'ticker': 'BEAV',
        #         'action': 'BUY',
        #         'price': beav['ask'],
        #         'size': size,
        #         'type': 'arbitrage'
        #     })
        #     order_list.append({
        #         'ticker': 'MAPL',
        #         'action': 'BUY',
        #         'price': mapl['ask'],
        #         'size': size,
        #         'type': 'arbitrage'
        #     })
        #     order_list.append({
        #         'ticker': 'COLD',
        #         'action': 'BUY',
        #         'price': cold['ask'],
        #         'size': size,
        #         'type': 'arbitrage'
        #     })
        # elif cnd['bid'] - beav['ask'] - 2 * mapl['ask'] - 2 * cold['ask'] < 0:
        #     print("Arbitraging with ETF!")
        #     size = (self.limits['gross_limit'] - self.limits['gross']) / 10
        #     order_list.append({
        #         'ticker': 'CND',
        #         'action': 'BUY',
        #         'price': cnd['ask'],
        #         'size': size,
        #         'type': 'arbitrage'
        #     })
        #     order_list.append({
        #         'ticker': 'BEAV',
        #         'action': 'SELL',
        #         'price': beav['bid'],
        #         'size': size,
        #         'type': 'arbitrage'
        #     })
        #     order_list.append({
        #         'ticker': 'MAPL',
        #         'action': 'SELL',
        #         'price': mapl['bid'],
        #         'size': size,
        #         'type': 'arbitrage'
        #     })
        #     order_list.append({
        #         'ticker': 'COLD',
        #         'action': 'SELL',
        #         'price': cold['bid'],
        #         'size': size,
        #         'type': 'arbitrage'
        #     })
        for sec in self.securities:
            position = sec['position']
            ticker = sec['ticker']
            bid = sec['bid']
            ask = sec['ask']
            spread = ask - bid
            top_bid = list(filter(lambda x: x['price'] == bid, self.book[ticker]['bid']))
            top_bid_size = reduce(lambda x, y: x + y['size'], [0] + top_bid)
            top_ask = list(filter(lambda x: x['price'] == ask, self.book[ticker]['ask']))
            top_ask_size = reduce(lambda x, y: x + y['size'], [0] + top_ask)
            mid = bid + spread / 2
            mid_floor = math.floor(mid * 100) / 100
            mid_ceil = math.ceil(mid * 100) / 100
            tranche_num = int((mid_floor - bid) / 0.01 - 1)
            total_size = min(top_bid_size, top_ask_size, (self.limits['gross_limit'] - self.limits['gross']) / 4)
            if ticker == 'CND':
                total_size = total_size / 5
            tranche_size = 0 if tranche_num == 0 else int(math.floor(total_size / tranche_num))

            if spread < 0:
                # Some arbitrage here
                print("Arbitrage opportunity found!")
                order_list.append({
                    'ticker': ticker,
                    'action': 'BUY',
                    'price': ask,
                    'size': min(top_bid_size, top_ask_size),
                    'type': 'arbitrage'
                })
                order_list.append({
                    'ticker': ticker,
                    'action': 'SELL',
                    'price': bid,
                    'size': min(top_bid_size, top_ask_size),
                    'type': 'arbitrage'
                })
            elif spread > 0.01:
                # Normal Market Making
                # print("Trying to make market")
                # print(ticker, spread, top_bid_size, top_ask_size, mid, tranche_num, tranche_size)
                if self.limits['gross_utilization'] < 0.8 and self.limits['net_utilization'] < 0.8:
                    print("Normal market making")
                    for i in range(0, tranche_num):
                        order_list.append({
                            'ticker': ticker,
                            'action': 'BUY',
                            'price': bid + 0.01 * i,
                            'size': tranche_size,
                            'type': 'market_making'
                        })
                        order_list.append({
                            'ticker': ticker,
                            'action': 'SELL',
                            'price': ask - 0.01 * i,
                            'size': tranche_size,
                            'type': 'market_making'
                        })
            # Additional adjustment order
            if position < 0:
                order_list.append({
                    'ticker': ticker,
                    'action': 'BUY',
                    'price': mid_floor,
                    'size': abs(position),
                    'type': 'inventory_management'
                })
            elif position > 0:
                order_list.append({
                    'ticker': ticker,
                    'action': 'SELL',
                    'price': mid_ceil,
                    'size': abs(position),
                    'type': 'inventory_management'
                })
        for o in order_list:
            if o['size']:
                self.send_order(o['ticker'], 'LIMIT', o['action'], o['size'], price=o['price'])

    def cancel_all_open_orders(self):
        order_id_list = []
        for o in self.orders:
            if o['status'] == 'OPEN':
                order_id_list.append(o['order_id'])
        for order_id in order_id_list:
            self.cancel_order(order_id)

    def liquidate_whole_inventory(self):
        print("Liquidating whole inventory!")
        order_list = []
        for sec in self.securities:
            position = sec['position']
            if position < 0:
                action = 'BUY'
            elif position > 0:
                action = 'SELL'
            else:
                continue
            order_list.append({
                'ticker': sec['ticker'],
                'action': action,
                'size': abs(position)
            })
        for o in order_list:
            self.send_order(o['ticker'], 'MARKET', o['action'], o['size'])

    def liquidate_inventory(self):
        order_list = []
        for sec in self.securities:
            position = sec['position']
            if position < 0:
                action = 'BUY'
                price = sec['ask']
            elif position > 0:
                action = 'SELL'
                price = sec['bid']
            else:
                continue

            if (sec['cost'] and sec['cost'] < sec['ask'] and action == 'BUY') or \
               (sec['cost'] and sec['cost'] > sec['bid'] and action == 'SELL'):
                order_list.append({
                    'ticker': sec['ticker'],
                    'action': action,
                    'size': abs(position),
                    'price': price
                })
        for o in order_list:
            self.send_order(o['ticker'], 'LIMIT', o['action'], o['size'], price=o['price'])

    def run(self):
        print("Initializing thread")
        while not thread_stop_event.isSet():
            if client_connect_event.isSet():  # Initial connect/new case
                self.get_update_case_tickers()
                client_connect_event.clear()
            try:
                if self.sync_tick() > 0:  # Per tick update
                    # Update tick
                    socketio.emit('tick', {'current_tick': self.current_tick}, namespace='/at')

                    # Update Orders
                    self.get_orders()

                    # Update Book
                    self.get_book()

                    # Update limits
                    self.get_limits()

                    # Cancel all open orders every 3 seconds
                    if self.current_tick % 3 == 0 and self.current_tick:
                        self.cancel_all_open_orders()

                    # Update securities
                    self.get_securities()

                    # Algo trading
                    if self.current_tick < 294:
                        # # If either gross utilization or net utilization > 95%, prioritize liquidation
                        # if self.liquidation_attempt > 3:
                        #     # 3 consecutive liquidation attempt - hard liquidate whole inventory
                        #     self.liquidate_whole_inventory()
                        #     self.liquidation_attempt = 0
                        # elif net_utilization > 0.95 or gross_utilization > 0.95:
                        #     self.liquidate_inventory()
                        #     self.liquidation_attempt += 1
                        # else:
                            self.algo_trading()
                    else:
                        # Clears inventory at the end
                        self.liquidate_whole_inventory()

                    # Maybe hard reset whole portfolio every 10 sec is a bad idea
                    # if self.current_tick % 10 == 0 and self.current_tick:
                    #     self.liquidate_whole_inventory()

                    if self.current_tick == 0:  # A new case started
                        client_connect_event.set()
                    socketio.sleep(self.delay)
            except ConnectionError:
                socketio.sleep(self.delay)
                continue


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/SalesTrader')
def sales_trader():
    # Only by sending this page first will the client be connected to the socketio instance
    return render_template("SalesTrader.html")


@app.route('/VolTrading')
def vol_trading():
    # Only by sending this page first will the client be connected to the socketio instance
    return render_template("VolTrading.html")


@app.route('/AlgoTrading')
def algo_trading():
    # Only by sending this page first will the client be connected to the socketio instance
    return render_template("AlgoTrading.html")


@socketio.on('connect', namespace='/snt')
def test_connect():
    # need visibility of the global objects
    global thread
    global client_connect_event

    print("Client connected to Sales & Trader Thread")
    client_connect_event.set()

    # Start the Sales & Trader thread only if the thread has not been started before.
    if not thread.isAlive():
        print("Starting Sales & Trader Thread")
        thread = SNTThread()
        thread.start()


@socketio.on('disconnect', namespace='/snt')
def test_disconnect():
    print('Client disconnected from Sales & Trader Thread')


@socketio.on('connect', namespace='/vt')
def test_connect():
    # need visibility of the global objects
    global thread
    global client_connect_event

    print("Client connected to Vol Trading Thread")
    client_connect_event.set()

    # Only start one thread at a time
    if not thread.isAlive():
        print("Starting Vol Trading Thread")
        thread = VTThread()
        thread.start()


@socketio.on('disconnect', namespace='/vt')
def test_disconnect():
    print('Client disconnected from Vol Trading Thread')


@socketio.on('connect', namespace='/at')
def test_connect():
    # need visibility of the global objects
    global thread
    global client_connect_event

    print("Client connected to Algo Trading Thread")
    client_connect_event.set()

    # Only start one thread at a time
    if not thread.isAlive():
        print("Starting Algo Trading Thread")
        thread = ATThread()
        thread.start()


@socketio.on('disconnect', namespace='/at')
def test_disconnect():
    print('Client disconnected from Algo Trading Thread')


if __name__ == '__main__':
    socketio.run(app, host="0.0.0.0", port=5001)
